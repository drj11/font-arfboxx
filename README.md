# Arfboxx

A pixel-ish font base on a simple triangular module, ◤.

A right-angled triangle that occupies the upper left half of a
rectangle.
In this embodiment the rectangle is 5mm×10mm.

The right-angled triangle is rotated through 180° so that the
lower-right version is also used.

The filled rectangle is also used, which can be thought as the
union of two triangles.

Effectively the font is designed on a pixel grid with a 1:2
aspect ratio where each pixel can be in one of 4 states:

- off
- top-left on (only)
- bottom-right on (only) ◢
- all on

![Alphabet](arfboxx-plaque.svg)

# END
